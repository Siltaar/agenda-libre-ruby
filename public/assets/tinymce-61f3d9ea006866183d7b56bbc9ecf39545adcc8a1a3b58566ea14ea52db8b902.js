(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      schema: 'html5',
      menubar: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-bd35f325ad91254feab551b82972c18f9d928b5016ff20ee040c3380f9c95a7c.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      toolbar: [' bold italic strikethrough | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | link image media insertdatetime charmap table | undo redo | searchreplace | code visualblocks preview fullscreen'],
      plugins: 'lists, advlist, autolink, link, image, charmap, paste, print, preview, table, fullscreen, searchreplace, media, insertdatetime, visualblocks, visualchars, wordcount, contextmenu, code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
