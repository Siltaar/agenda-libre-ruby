xml.instruct!

xml.events do
  @events.find_each do |event|
    xml.event do
      xml.title event.title
      xml.tag! 'start-time', event.start_time.strftime('%Y/%m/%d %T')
      xml.tag! 'end-time', event.end_time.strftime('%Y/%m/%d %T')
      xml.place_name event.place_name
      xml.address event.address
      xml.city event.city
      xml.region event.region
      xml.locality event.locality
      xml.tags event.tags
      xml.contact event.contact
      xml.adlurl event_url event
      xml.description { xml.cdata! event.description }
    end
  end
end
