(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      schema: 'html5',
      menubar: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-6054b0bc1c370f1c7d66e9d0c3d21aa9fa886547e9c7732570440a4f42618a99.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      toolbar: [' bold italic strikethrough | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | link image media insertdatetime charmap table | undo redo | searchreplace | code visualblocks preview fullscreen'],
      plugins: 'lists, advlist, autolink, link, image, charmap, paste, print, preview, table, fullscreen, searchreplace, media, insertdatetime, visualblocks, visualchars, wordcount, contextmenu, code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
