source 'https://rubygems.org'

# The central piece of this application: the month calendar view
gem 'simple_calendar'
# The recurrence management library
gem 'ice_cube'
# Rails engine for static pages
gem 'high_voltage'

gem 'rails'
# Later versions require a new ruby version, which is not yet in production
gem 'has_scope', '< 0.7'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

gem 'haml-rails'

gem 'devise'
gem 'devise-i18n'
gem 'http_accept_language'
gem 'rails-i18n'
# Store some specific content, like application name and static pages contents
gem 'i18n-active_record',
    github: 'svenfuchs/i18n-active_record',
    require: 'i18n/active_record'
# Remove leading and trailing white spaces
gem 'strip_attributes'
# Track changes to models' data
gem 'paper_trail'
gem 'piwik_analytics'

# Validate mails submitted
gem 'email_validator'
# Email address obfuscation
gem 'actionview-encoded_mail_to'
# To display a patched diff for event descriptions
gem 'differ'

# A generic library to administrate the tool
gem 'activeadmin', github: 'activeadmin'

# Markdown display
gem 'redcarpet'

gem 'geocoder'

# Tiny MCE integration
gem 'tinymce-rails'
gem 'tinymce-rails-langs'

gem 'acts-as-taggable-on'
gem 'tagsinput-rails'

# SEO optimisations
gem 'meta-tags'

gem 'twitter'

group :development do
  gem 'quiet_assets'
  gem 'sqlite3'
  gem 'webrick'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug'

  # Scanning the Gemfile for insecure dependencies based on published CVEs
  gem 'bundler-audit'

  gem 'guard-bundler', require: false
  gem 'guard-livereload', require: false
  gem 'guard-migrate', require: false
  gem 'guard-minitest', require: false
  gem 'guard-rubocop', require: false
  gem 'haml_lint'
  gem 'rack-livereload'
  # Security checks
  gem 'brakeman', require: false
  gem 'guard-brakeman', require: false
  gem 'guard-rails', require: false
end

group :test do
  gem 'simplecov'
end

group :development, :test do
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :development, :test, :assets do
  # Use SASS for stylesheets
  gem 'sass-rails'
  # Use CoffeeScript for .coffee assets and views
  gem 'coffee-rails'

  # Use jquery as the JavaScript library
  gem 'jquery-rails'
  # Turbolinks makes following links in your web application
  # faster. Read more: https://github.com/rails/turbolinks
  gem 'turbolinks'
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', group: :doc

  gem 'modernizr-rails'

  gem 'jquery-sparkline-rails', github: 'cubus/jquery-sparkline-rails'

  # A superb font to use as icons
  gem 'font-awesome-sass'

  # Carte openstreetmap
  gem 'leaflet-rails'
  gem 'rails-assets-leaflet.markercluster', source: 'https://rails-assets.org'
end

group :assets do
  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier'
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', platforms: :ruby
end

group :production do
  # Use mysql as the database for Active Record
  gem 'mysql2'
end
